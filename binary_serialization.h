/*
 * Copyright (C) 2014 Lasse Schuirmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BINARY_SERIALIZATION_H
#define _BINARY_SERIALIZATION_H

#include <stdint.h>

#include "serializable.h"

/**
 * Writes a binary type value to a stream.
 * 
 * @param os Reference to the stream.
 * @param value Reference to the value (const).
 * @return The stream for further operations.
 */
template <typename T>
inline std::ostream&
binary_write (std::ostream& os, const T& value)
{
  return os.write (reinterpret_cast<const char*>(&value), sizeof (value));
}

/**
 * Read a binary type value from stream.
 * 
 * @param is Reference to the stream.
 * @param value Reference to the value (will be changed).
 * @return The stream for further operations.
 */
template <typename T>
inline std::istream&
binary_read (std::istream& is, T& value)
{
  return is.read (reinterpret_cast<char*>(&value), sizeof (value));
}

// wrappers for common types

inline std::ostream&
basic_write (std::ostream& os, const bool& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const uint8_t& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const uint16_t& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const uint32_t& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const uint64_t& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const int8_t& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const int16_t& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const int32_t& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const int64_t& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const float& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const double& value)
{
  return binary_write (os, value);
}

inline std::ostream&
basic_write (std::ostream& os, const char& value)
{
  return binary_write (os, value);
}



inline std::istream&
basic_read (std::istream& is, bool& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, uint8_t& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, uint16_t& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, uint32_t& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, uint64_t& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, int8_t& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, int16_t& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, int32_t& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, int64_t& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, float& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, double& value)
{
  return binary_read (is, value);
}

inline std::istream&
basic_read (std::istream& is, char& value)
{
  return binary_read (is, value);
}

#endif // _BINARY_SERIALIZATION_H
