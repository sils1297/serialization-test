# Copyright (C) 2014 Lasse Schuirmann
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

GPP = g++
RM  = rm
LD  = ld

SRCS	= $(shell find ./ -name '*.cpp')
OBJFILES= $(addsuffix .o,$(basename $(SRCS)))

GFLAGS = -Wall -pedantic -Wextra

# Colors
GREEN      = \033[1;32m
RED        = \033[1;31m
YELLOW     = \033[1;33m
NORMAL     = \033[0m
BLUE       = \033[1;34m

default: proof

all: clean proof
rebuild: all

.cpp.o:
	@printf " GPP : $(YELLOW)Compiling $@ from $<...$(NORMAL)"
	@$(GPP) $(GFLAGS) -o $@ -c $<
	@printf " $(GREEN)Done.$(NORMAL)\n"

proof: $(OBJFILES)
	@printf " GPP : $(BLUE)Linking $@ from object files...$(NORMAL)"
	@$(GPP) -o $@ $^
	@printf " $(GREEN)Done.$(NORMAL)\n"

run: proof
	@printf "     : $(GREEN)Running program...$(NORMAL)\n"
	@./proof

clean:
	@printf " RM  : $(RED)Removing all generated files...$(NORMAL)"
	@$(RM) -rf $(OBJFILES) proof test.db
	@printf " $(GREEN)Done.$(NORMAL)\n"
