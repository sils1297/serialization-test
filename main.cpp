/*
 * Copyright (C) 2014 Lasse Schuirmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>

#include <string.h>

#include "proof_base.h"
#include "proof_child.h"
#include "another_child.h"
#include "basic_serialization.h"

#define       FILE      "test.db"

int
main ()
  {
    // construct copies
    another_child cp (0, 0);
    proof_base base_exmpl (0,0);
    
    // construct example
    proof_child inst (3, 1.5);
    inst.set_integer (4);
    inst.set_childs_int (42);
    inst.set_string ("test");
    
    std::cout << "Original:" << std::endl;
    inst.print ();
    
    // serialize via serializable class
    serializable& instref = inst;
    std::ofstream seri (FILE, std::ofstream::out | std::ofstream::binary);
    std::cout << "\nSerializing original..." << std::endl;
    write_with_id (seri, instref);
    seri.close ();
    
    // let proof_base construct the right derivative
    std::ifstream deseri3 (FILE, std::ifstream::in | std::ifstream::binary);
    std::cout << "\nLet proof_base construct the right derivative and "
              << "deserialize it..." << std::endl;
    proof_base* autoder = proof_base::read_derivative (deseri3);
    deseri3.close ();
    
    std::cout << "proof_base copy with class detection:" << std::endl;
    autoder->print ();
    
    // deserialize backward to base class
    std::ifstream deseri2 (FILE, std::ifstream::in | std::ifstream::binary);
    std::cout << "\nDeserializing to base-class copy..." << std::endl;
    read_with_id (deseri2, base_exmpl);
    deseri2.close ();
    
    std::cout << "Base-class copy:" << std::endl;
    base_exmpl.print ();
    
    // deserialize via original class
    std::ifstream deseri (FILE, std::ifstream::in | std::ifstream::binary);
    std::cout << "\nDeserializing to another_child copy..." << std::endl;
    read_with_id (deseri, cp);
    deseri.close ();
    
    std::cout << "Another_child copy:" << std::endl;
    cp.print ();
  }
