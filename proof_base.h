/*
 * Copyright (C) 2014 Lasse Schuirmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PROOF_BASE_H
#define _PROOF_BASE_H

#include <list>
#include <iostream>

#include <stdint.h>

#include "serializable.h"
#include "basic_serialization.h"

// All derivatives need to have a unique id
typedef enum
{
  PROOF_BASE,
  PROOF_CHILD,
  ANOTHER_CHILD
} SER_IDS;

class
proof_base : public serializable
{
public:
  proof_base (uint8_t count = 0, float value = 0.0);
  ~proof_base ();
  
  void set_integer (const int value);
  
  /**
   * Sets the string.
   * 
   * @return false if input string is too long.
   */
  bool set_string (const char *value);
  
  /**
   * Prints the contents of all member variables.
   */
  virtual void print() const;
  
  static proof_base* read_derivative (std::istream& is);
  
  virtual inline std::ostream& write (std::ostream& os) const
    {
      basic_write (os, an_integer);
      basic_write (os, a_string);
      return basic_write (os, a_list);
    }
  
  virtual inline std::istream& read (std::istream& is)
    {
      basic_read (is, an_integer);
      basic_read (is, a_string, 100);
      return basic_read (is, a_list);
    }
  
  virtual inline std::istream& read_base (std::istream& is)
    {
      return proof_base::read (is);
    }
protected:
  /// An example integer member.
  int an_integer;
  char* a_string;
  /// An example list of float
  std::list<float> a_list;
};

#endif // _PROOF_BASE_H
