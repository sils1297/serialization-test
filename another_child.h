/*
 * Copyright (C) 2014 Lasse Schuirmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ANOTHER_CHILD_H
#define _ANOTHER_CHILD_H

#include <stdint.h>

#include "proof_base.h"

class
another_child : public proof_base
{
public:
  another_child (uint8_t count = 0, float value = 0.0);
  
  void print() const;
  
  inline std::ostream& write (std::ostream& os) const
    {
      proof_base::write (os);
      return basic_write (os, childs_chr);
    }
  
  inline std::istream& read (std::istream& is)
    {
      proof_base::read (is);
      return basic_read (is, childs_chr);
    }
protected:
  char childs_chr;
};

#endif // _ANOTHER_CHILD_H
