/*
 * Copyright (C) 2014 Lasse Schuirmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SERIALIZABLE_H
#define _SERIALIZABLE_H

#include <iostream>
#include "binary_serialization.h"

/**
 * Assumption: we do not have more than 255 different class ids.
 */
typedef uint8_t     CLASS_ID_TYPE;
/**
 * A value of CLASS_ID_TYPE that means undefined.
 */
#define UNDEFINED   static_cast<CLASS_ID_TYPE>(-1)

class
serializable
{
public:
  serializable ()
    : c_unique_id(UNDEFINED)
    { }
  virtual ~serializable () { };
  
  /**
   * Serializes the object to a binary stream.
   * 
   * @param os The stream to write into.
   * @return The stream as usual.
   */
  virtual std::ostream& write (std::ostream& os) const = 0;
  
  /**
   * Serializes the object to a binary stream. This version will not write the
   * classid as it may be implicitly known.
   * 
   * @param os The stream to write into.
   * @return The stream as usual.
   */
  virtual std::ostream& write_with_id (std::ostream& os) const
    {
      if (c_unique_id == UNDEFINED)
        {
          std::cout << "This is a bug. Let the developer know that he has to"
                    << " define the c_unique_id of is serialization object."
                    << std::endl;
          return os;
        }
      basic_write (os, c_unique_id);
      return write (os);
    }
  
  /**
   * Loads a serialized object from binary stream.
   * 
   * @param is The stream to write into.
   * @return The stream as usual.
   */
  virtual std::istream& read (std::istream& is) = 0;
  
  /**
   * Deserializes the base class. Overwrite this method in the base class of
   * all classes you want to (de)serialize. It will be invoked if the child
   * can not be identified.
   */
  virtual std::istream& read_base (std::istream& is)
    {
      std::cout << "read_base is not implemented. Base class can not be read."
                << std::endl;
      return is;
    }
  
  /**
   * Loads a serialized object from binary stream. This version will not read
   * the classid as it may be implicitly known.
   * 
   * @param is The stream to write into.
   * @return The stream as usual.
   */
  virtual std::istream& read_with_id (std::istream& is)
    {
      CLASS_ID_TYPE classid;
      basic_read (is, classid);
      if (classid != c_unique_id)
        {
          std::cout << "Wrong class id (" << static_cast<int>(classid)
                    << "). Trying to read base class..." << std::endl;
          return read_base (is);
        }
      return read (is);
    }
protected:
  CLASS_ID_TYPE c_unique_id;
};

#endif // _SERIALIZABLE_H
