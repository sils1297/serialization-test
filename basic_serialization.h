/*
 * Copyright (C) 2014 Lasse Schuirmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BASIC_SERIALIZATION_H
#define _BASIC_SERIALIZATION_H

#include <iostream>
#include <string>

#include <string.h>
#include <stdint.h>
#include "serializable.h"
#include "binary_serialization.h"

/**
 * This file provides binary serialization and deserialiation for some binary,
 * STL and all serializable classes.
 */

// to save information if this is a null pointer or not - used like class ids
#define NULL_POINTER        static_cast<uint8_t>(0)
#define NON_NULL_POINTER    static_cast<uint8_t>(1)

/// Some of these functions may be unused. We don't need the warning.
#define UNUSED              __attribute__((unused))

// -- PROTOTYPES

// serializable
/**
 * basic_write for serializable classes.
 * 
 * @param os Reference to the stream.
 * @param value Reference to the value (const).
 */
inline std::ostream& basic_write (std::ostream& os, const serializable& value);

/**
 * basic_read for serializable classes.
 * 
 * @param is Reference to the stream.
 * @param value Reference to the value (will be changes)).
 */
inline std::istream& basic_read (std::istream& is, serializable& value);

// C string
inline std::ostream& basic_write (std::ostream& os, const char* value);

/**
 * There is no ordinary specialization for this since this would be good for
 * buffer overflows and all these evil things.
 */
static std::istream& basic_read (std::istream& is, char* value,
                          size_t max_length);

// std::string
inline std::ostream& basic_write (std::ostream& os, const std::string value);

static std::istream& basic_read (std::istream& is, std::string value);

// Lists
template <typename N>
inline std::ostream& basic_write (std::ostream& os, const std::list<N>& value);

template <typename N>
inline std::istream& basic_read (std::istream& is, std::list<N>& value);

// Pointers
template <class Z>
inline std::ostream& basic_write (std::ostream& os, const Z* value);

template <typename Z>
inline std::istream& basic_read (std::istream& is, Z* value);

//TODO write declarations
inline std::ostream& write_with_id (std::ostream& os, const serializable& value)
{
  return value.write_with_id (os);
}

inline std::istream& read_with_id (std::istream& is, serializable& value)
{
  return value.read_with_id (is);
}


// serializable
inline std::ostream&
basic_write (std::ostream& os, const serializable& value)
{
  return value.write (os);
}

inline std::istream&
basic_read (std::istream& is, serializable& value)
{
  return value.read (is);
}

// C strings
inline std::ostream&
basic_write (std::ostream& os, const char* value)
{
  // for maximum performance just calculate the length once and omit the
  // termination character
  size_t len = strlen (value);
  basic_write (os, len);
  return os.write (value, len);
}

/**
 * Overload for good old C strings.
 * 
 * There is no ordinary specialization for this since this would be good for
 * buffer overflows and all these evil things.
 */
UNUSED std::istream&
basic_read (std::istream& is, char* value, size_t max_length)
{
  size_t len;
  basic_read (is, len);
  if (len <= max_length)
    {
      is.read (value, len);
      // this was not in the serialized version
      value[len] = '\0';
    }
  else
    {
      std::cerr << "The file is probably corrupted." << std::endl;
      throw -1; // TODO
    }
  return is;
}

/**
 * std::string overload
 */
inline std::ostream&
basic_write (std::ostream& os, const std::string value)
{
  return basic_write (os, value.c_str());
}

UNUSED std::istream&
basic_read (std::istream& is, std::string value)
{
  
  size_t len;
  basic_read (is, len);
  char * tmp = new char[len + 1];
  
  is.read (tmp, len);
  // this was not in the serialized version
  tmp[len] = '\0';
  
  value = tmp;// convert to std::string
  return is;
}

/**
 * Template for lists.
 */
template <typename N>
inline std::ostream&
basic_write (std::ostream& os, const std::list<N>& value)
{
  basic_write (os, value.size ());
  typename std::list<N>::const_iterator it = value.begin(), endit = value.end ();
  for (; it != endit; ++it)
    {
      basic_write (os, *it);
    }
  return os;
}

/**
 * Template for lists.
 */
template <typename N>
inline std::istream&
basic_read (std::istream& is, std::list<N>& value)
{
  typename std::list<N>::size_type size;
  N tmp;
  basic_read (is, size);
  while (size > 0)
    {
      basic_read (is, tmp);
      value.push_back (tmp);
      --size;
    }
  return is;
}

// Dereference pointers when reading or writing
template <class Z>
inline std::ostream&
basic_write (std::ostream& os, const Z* value)
{
  if (value != NULL)
    {
      basic_write (os, NON_NULL_POINTER);
      return basic_write (os, *value);
    }
  else
    {
      basic_write (os, NULL_POINTER);
      return os;
    }
}

template <typename Z>
inline std::istream&
basic_read (std::istream& is, Z* value)
{
  uint8_t typ;
  basic_read (is, typ);
  if (typ == NULL_POINTER)
    {
      return is;
    }
  else
    {
      if (value != NULL)
        {
          
          return basic_read (is, *value);
        }
      else
        {
          // Null pointer exception
          throw -1;
          return is;
        }
    }
}

#endif // _BASIC_SERIALIZATION_H
