/*
 * Copyright (C) 2014 Lasse Schuirmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "proof_child.h"
#include <iostream>

proof_child::proof_child (uint8_t count, float value)
  : proof_base (count, value)
{
  c_unique_id = PROOF_CHILD;
}

void proof_child::set_childs_int (const int value)
{
  childs_int = value;
}

void
proof_child::print() const
{
  std::cout << "  type      : proof_child" << std::endl;
  std::cout << "  an_integer: " << an_integer << std::endl;
  std::cout << "  a_string  : " << a_string << std::endl;
  std::cout << "  childs_int: " << childs_int << std::endl;
  std::cout << "  a_list    : " << a_list.size () << std::endl;
  std::list<float>::const_iterator it = a_list.begin ();
  for (; it != a_list.end (); it++)
    std::cout << "              : " << *it << std::endl;
}