/*
 * Copyright (C) 2014 Lasse Schuirmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "another_child.h"
#include <iostream>

another_child::another_child (uint8_t count, float value)
: proof_base (count, value), childs_chr('U')
{
  c_unique_id = ANOTHER_CHILD;
}

void
another_child::print() const
{
  std::cout << "  type      : another_child" << std::endl;
  std::cout << "  an_integer: " << an_integer << std::endl;
  std::cout << "  a_string  : " << a_string << std::endl;
  std::cout << "  childs_chr: " << childs_chr << std::endl;
  std::cout << "  a_list    : " << a_list.size () << std::endl;
  std::list<float>::const_iterator it = a_list.begin ();
  for (; it != a_list.end (); it++)
    std::cout << "              : " << *it << std::endl;
}