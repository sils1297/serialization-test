/*
 * Copyright (C) 2014 Lasse Schuirmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "proof_base.h"

#include "proof_child.h"
#include "another_child.h"

proof_base::proof_base (uint8_t count, float value)
  : a_list (count, value)
{
  a_string = new char[100];
  c_unique_id = PROOF_BASE;
}

proof_base::~proof_base ()
{
  delete[] a_string;
}

void proof_base::set_integer (const int value)
{
  an_integer = value;
}

bool proof_base::set_string (const char *value)
{
  if (strlen (value) > 99)
    {
      return false;
    }
  strcpy (a_string, value);
  return true;
}

void
proof_base::print() const
{
  std::cout << "  type      : proof_base" << std::endl;
  std::cout << "  an_integer: " << an_integer << std::endl;
  std::cout << "  a_string  : " << a_string << std::endl;
  std::cout << "  a_list    : " << a_list.size () << std::endl;
  std::list<float>::const_iterator it = a_list.begin ();
  for (; it != a_list.end (); it++)
    std::cout << "              : " << *it << std::endl;
}

proof_base*
proof_base::read_derivative (std::istream& is)
{
  proof_base* result;
  CLASS_ID_TYPE classid;
  binary_read (is, classid);
  switch (classid)
    {
    case PROOF_CHILD:
      result = new proof_child ();
      break;
    case ANOTHER_CHILD:
      result = new another_child ();
      break;
    case PROOF_BASE:
      result = new proof_base ();
      break;
    default: // read base class instead
      std::cout << "No valid class id found (" << (int)classid<<"). (Maybe it's just unimplemented?) "
                << "Deserialize proof_base instead." << std::endl;
      result = new proof_base (0, 0);
      break;
    }
  result->read (is);
  return result;
}
